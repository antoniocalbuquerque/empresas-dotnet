USE [master]
GO
/****** Object:  Database [EmpresasDB]    Script Date: 22/12/2019 23:42:32 ******/
CREATE DATABASE [EmpresasDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'EmpresasDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER01\MSSQL\DATA\EmpresasDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'EmpresasDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER01\MSSQL\DATA\EmpresasDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [EmpresasDB] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EmpresasDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EmpresasDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [EmpresasDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [EmpresasDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [EmpresasDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [EmpresasDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [EmpresasDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [EmpresasDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [EmpresasDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [EmpresasDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [EmpresasDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [EmpresasDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [EmpresasDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [EmpresasDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [EmpresasDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [EmpresasDB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [EmpresasDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [EmpresasDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [EmpresasDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [EmpresasDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [EmpresasDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [EmpresasDB] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [EmpresasDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [EmpresasDB] SET RECOVERY FULL 
GO
ALTER DATABASE [EmpresasDB] SET  MULTI_USER 
GO
ALTER DATABASE [EmpresasDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [EmpresasDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [EmpresasDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [EmpresasDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [EmpresasDB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'EmpresasDB', N'ON'
GO
ALTER DATABASE [EmpresasDB] SET QUERY_STORE = OFF
GO
USE [EmpresasDB]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 22/12/2019 23:42:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Enterprise]    Script Date: 22/12/2019 23:42:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Enterprise](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[EnterpriseTypeId] [int] NOT NULL,
 CONSTRAINT [PK_Enterprise] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EnterpriseType]    Script Date: 22/12/2019 23:42:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EnterpriseType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_EnterpriseType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 22/12/2019 23:42:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191222234325_Initial', N'2.1.11-servicing-32099')
SET IDENTITY_INSERT [dbo].[Enterprise] ON 

INSERT [dbo].[Enterprise] ([Id], [Name], [EnterpriseTypeId]) VALUES (1, N'Submarino', 19)
INSERT [dbo].[Enterprise] ([Id], [Name], [EnterpriseTypeId]) VALUES (2, N'Americanas', 19)
INSERT [dbo].[Enterprise] ([Id], [Name], [EnterpriseTypeId]) VALUES (3, N'Amazon', 19)
INSERT [dbo].[Enterprise] ([Id], [Name], [EnterpriseTypeId]) VALUES (4, N'Subway', 15)
INSERT [dbo].[Enterprise] ([Id], [Name], [EnterpriseTypeId]) VALUES (5, N'E&ASPORTS', 14)
INSERT [dbo].[Enterprise] ([Id], [Name], [EnterpriseTypeId]) VALUES (6, N'Microsoft', 3)
INSERT [dbo].[Enterprise] ([Id], [Name], [EnterpriseTypeId]) VALUES (7, N'ioasys', 2)
INSERT [dbo].[Enterprise] ([Id], [Name], [EnterpriseTypeId]) VALUES (8, N'Google', 2)
INSERT [dbo].[Enterprise] ([Id], [Name], [EnterpriseTypeId]) VALUES (9, N'CVC', 24)
SET IDENTITY_INSERT [dbo].[Enterprise] OFF
SET IDENTITY_INSERT [dbo].[EnterpriseType] ON 

INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (1, N'Health')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (2, N'Technology')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (3, N'Software')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (4, N'Social')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (5, N'Smart City')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (6, N'Service')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (7, N'Real Estate')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (8, N'Products')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (9, N'Mining')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (10, N'Media')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (11, N'Logistics')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (12, N'IOT')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (13, N'Transport')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (14, N'Games')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (15, N'Food')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (16, N'Fintech')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (17, N'Fashion')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (18, N'Education')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (19, N'Ecommerce')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (20, N'Eco')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (21, N'Biotech')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (22, N'Aviation')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (23, N'Agro')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (24, N'Tourism')
SET IDENTITY_INSERT [dbo].[EnterpriseType] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([Id], [Email], [Password]) VALUES (1, N'usr02@gmail.com', N'654321')
INSERT [dbo].[User] ([Id], [Email], [Password]) VALUES (2, N'usr01@gmail.com', N'123456')
SET IDENTITY_INSERT [dbo].[User] OFF
/****** Object:  Index [IX_Enterprise_EnterpriseTypeId]    Script Date: 22/12/2019 23:42:33 ******/
CREATE NONCLUSTERED INDEX [IX_Enterprise_EnterpriseTypeId] ON [dbo].[Enterprise]
(
	[EnterpriseTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Enterprise]  WITH CHECK ADD  CONSTRAINT [FK_Enterprise_EnterpriseType_EnterpriseTypeId] FOREIGN KEY([EnterpriseTypeId])
REFERENCES [dbo].[EnterpriseType] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Enterprise] CHECK CONSTRAINT [FK_Enterprise_EnterpriseType_EnterpriseTypeId]
GO
USE [master]
GO
ALTER DATABASE [EmpresasDB] SET  READ_WRITE 
GO
