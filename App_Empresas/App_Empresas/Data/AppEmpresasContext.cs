﻿using Microsoft.EntityFrameworkCore;
using App_Empresas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Empresas.Data
{
    public class AppEmpresasContext : DbContext
    {
        public AppEmpresasContext()
        {
            
        }

        public AppEmpresasContext(DbContextOptions<AppEmpresasContext> options)
            : base(options)
        {
        }

        public DbSet<User> User { get; set; }
        public DbSet<Enterprise> Enterprise { get; set; }
        public DbSet<EnterpriseType> EnterpriseType { get; set; }
    }
}
