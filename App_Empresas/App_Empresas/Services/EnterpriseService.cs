﻿using App_Empresas.Data;
using App_Empresas.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Empresas.Services
{
    public class EnterpriseService
    {
        private readonly AppEmpresasContext _context;

        public EnterpriseService(AppEmpresasContext context)
        {
            _context = context;
        }

        public List<Enterprise> FindEnterpriseAll()
        {
            return _context.Enterprise.ToList();
        }

        public Enterprise FindEnterpriseById(int id)
        {
            return _context.Enterprise.Include(obj => obj.EnterpriseType).FirstOrDefault(obj => obj.Id == id);
        }

        public List<EnterpriseType> FindEnterpriseTypesAll()
        {
            return _context.EnterpriseType.ToList();
        }

        public List<Enterprise> FindEnterpriseByTypeId(int typeId)
        {
            return _context.Enterprise.Include(obj => obj.EnterpriseType).Where(obj => obj.EnterpriseTypeId == typeId).ToList();
        }

        public List<Enterprise> FindEnterpriseByTypeName(string typeName)
        {
            int typeId = _context.EnterpriseType.Where(obj => obj.Name == typeName).Select(et => et.Id).FirstOrDefault();
            return _context.Enterprise.Include(obj => obj.EnterpriseType).Where(obj => obj.EnterpriseTypeId == typeId).ToList();
        }
    }
}
