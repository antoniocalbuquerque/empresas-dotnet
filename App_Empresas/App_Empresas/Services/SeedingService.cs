﻿using System;
using System.Collections.Generic;
using System.Linq;  
using System.Threading.Tasks;
using App_Empresas.Models;
using App_Empresas.Data;

namespace App_Empresas.Services
{
    public class SeedingService
    {
        private readonly AppEmpresasContext _context;

        public SeedingService(AppEmpresasContext context)
        {
            _context = context;
        }

        public void Seed()
        {
            if ( _context.User.Any() && _context.EnterpriseType.Any() && _context.Enterprise.Any() )                
            {
                return;
            }

            User u1 = new User("usr01@gmail.com", "123456");
            User u2 = new User("usr02@gmail.com", "654321");

            EnterpriseType et1 = new EnterpriseType("Agro");
            EnterpriseType et2 = new EnterpriseType("Aviation");
            EnterpriseType et3 = new EnterpriseType("Biotech");
            EnterpriseType et4 = new EnterpriseType("Eco");
            EnterpriseType et5 = new EnterpriseType("Ecommerce");
            EnterpriseType et6 = new EnterpriseType("Education");
            EnterpriseType et7 = new EnterpriseType("Fashion");
            EnterpriseType et8 = new EnterpriseType("Fintech");
            EnterpriseType et9 = new EnterpriseType("Food");
            EnterpriseType et10 = new EnterpriseType("Games");
            EnterpriseType et11 = new EnterpriseType("Health");
            EnterpriseType et12 = new EnterpriseType("IOT");
            EnterpriseType et13 = new EnterpriseType("Logistics");
            EnterpriseType et14 = new EnterpriseType("Media");
            EnterpriseType et15 = new EnterpriseType("Mining");
            EnterpriseType et16 = new EnterpriseType("Products");
            EnterpriseType et17 = new EnterpriseType("Real Estate");
            EnterpriseType et18 = new EnterpriseType("Service");
            EnterpriseType et19 = new EnterpriseType("Smart City");
            EnterpriseType et20 = new EnterpriseType("Social");
            EnterpriseType et21 = new EnterpriseType("Software");
            EnterpriseType et22 = new EnterpriseType("Technology");
            EnterpriseType et23 = new EnterpriseType("Tourism");
            EnterpriseType et24 = new EnterpriseType("Transport");

            Enterprise e1 = new Enterprise("E&ASPORTS",et10);
            Enterprise e2 = new Enterprise("CVC", et23);
            Enterprise e3 = new Enterprise("Microsoft", et21);
            Enterprise e4 = new Enterprise("ioasys", et22);            
            Enterprise e5 = new Enterprise("Subway", et9);
            Enterprise e6 = new Enterprise("Submarino", et5);
            Enterprise e7 = new Enterprise("Americanas", et5);
            Enterprise e8 = new Enterprise("Amazon", et5);
            Enterprise e9 = new Enterprise("Google", et22);

            _context.User.AddRange(u1, u2);

            _context.EnterpriseType.AddRange(et1, et2, et3, et4, et5, et6, et7, et8, et9, et10, et11, et12, et13, et14, et15, et16, et17, et18, et19, et20, et21, et22, et23, et24);

            _context.Enterprise.AddRange(e1, e2, e3, e4, e5, e6, e7, e8, e9);                      

            _context.SaveChanges();
        }
    }
}
