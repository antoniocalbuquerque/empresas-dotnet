﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App_Empresas.Data;
using App_Empresas.Models;

namespace App_Empresas.Services
{
    public class UserService
    {
        private readonly AppEmpresasContext _context;

        public UserService (AppEmpresasContext context)
        {
            _context = context;
        }

        public User Find(string email, string password)
        {
            return _context.User.FirstOrDefault(u => u.Email == email && u.Password == password);                                
        }
    }
}
