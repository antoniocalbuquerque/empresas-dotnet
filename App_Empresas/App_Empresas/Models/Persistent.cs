﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace App_Empresas.Models
{
    public abstract class Persistent
    {
        [JsonIgnore]                
        public int Id { get; set; }

        public Persistent()
        {

        }
    }
}
