﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace App_Empresas.Models
{
    public class Enterprise : Persistent
    {        
        public string Name { get; set; }

        public int EnterpriseTypeId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EnterpriseType EnterpriseType { get; set; }

        public Enterprise()
        {

        }

        public Enterprise(string name, EnterpriseType enterpriseType)
        {            
            Name = name;
            EnterpriseType = enterpriseType;
        }
    }
}
