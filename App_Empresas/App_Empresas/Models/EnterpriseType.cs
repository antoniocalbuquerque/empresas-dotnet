﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Empresas.Models
{
    public class EnterpriseType : Persistent
    {        
        public string Name { get; set; }
       
        public EnterpriseType()
        {

        }

        public EnterpriseType(string name)
        {    
            Name = name;
        }
    }
}
