﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.IdentityModel.Tokens;
using App_Empresas.Configurations;
using App_Empresas.Models;
using App_Empresas.Services;

namespace App_Empresas.Controllers
{
    /// <summary>
    /// Controller para autenticação e geração do token
    /// </summary>
    [Route("api/auth/sign_in")]
    public class AuthenticationController : Controller
    {
        private readonly UserService _userService;
        private readonly SigningConfiguration _signingConfiguration;
        private readonly TokenConfiguration _tokenConfiguration;

        public AuthenticationController(UserService userService, SigningConfiguration signingConfiguration, TokenConfiguration tokenConfiguration)
        {
            _userService = userService;
            _signingConfiguration = signingConfiguration;
            _tokenConfiguration = tokenConfiguration;
        }

        /// <summary>
        /// Geração do token para autenticação
        /// </summary>
        [AllowAnonymous]
        [HttpPost]
        public object Post([FromBody]User user)
        {
            bool userOK = false;
            if (user != null && !String.IsNullOrWhiteSpace(user.Email) && !String.IsNullOrWhiteSpace(user.Password))
            {
                var usuarioBase = _userService.Find(user.Email, user.Password);
                userOK = (usuarioBase != null && user.Email == usuarioBase.Email && user.Password == usuarioBase.Password);
            }

            if (userOK)
            {
                ClaimsIdentity identity = new ClaimsIdentity(
                    new GenericIdentity(user.Email, "Login"),
                    new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.UniqueName, user.Email)
                    }
                );

                DateTime dataCriacao = DateTime.Now;
                DateTime dataExpiracao = dataCriacao +
                    TimeSpan.FromSeconds(_tokenConfiguration.Seconds);

                var handler = new JwtSecurityTokenHandler();
                var securityToken = handler.CreateToken(new SecurityTokenDescriptor
                {
                    Issuer = _tokenConfiguration.Issuer,
                    Audience = _tokenConfiguration.Audience,
                    SigningCredentials = _signingConfiguration.SigningCredentials,
                    Subject = identity,
                    NotBefore = dataCriacao,
                    Expires = dataExpiracao
                });
                var token = handler.WriteToken(securityToken);

                return new
                {
                    authenticated = true,
                    created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                    expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                    accessToken = token,
                    message = "OK"
                };
            }
            else
            {
                return new
                {
                    authenticated = false,
                    message = "Falha ao autenticar"
                };
            }
        }
    }
}