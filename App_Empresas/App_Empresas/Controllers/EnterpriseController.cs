﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using App_Empresas.Data;
using App_Empresas.Models;
using App_Empresas.Services;
using Microsoft.AspNetCore.Authorization;

namespace App_Empresas.Controllers
{
    /// <summary>
    /// Controller para consulta de Informações de Empresas
    /// </summary>
    [Route("api/enterprises")]
    [ApiController]
    public class EnterpriseController : ControllerBase
    {       
        private readonly EnterpriseService _enterpriseService;

        public EnterpriseController(EnterpriseService enterpriseService)
        {            
            _enterpriseService = enterpriseService;
        }

        /// <summary>
        /// Lista todas as Empresas
        /// </summary>                
        [Authorize("Bearer")]
        [HttpGet]
        public ActionResult<IEnumerable<Enterprise>> GetEnterprise()
        {
            var listEnterprise = _enterpriseService.FindEnterpriseAll();

            if (listEnterprise.Count == 0)
            {
                return NotFound();
            }

            return Ok(listEnterprise);
        }

        /// <summary>
        /// Consulta Empresa pelo Id
        /// </summary>
        [Authorize("Bearer")]
        [HttpGet("{id}")]
        public ActionResult<Enterprise> GetEnterprise([FromRoute] int id)
        {            
            var enterprise = _enterpriseService.FindEnterpriseById(id);

            if (enterprise == null)
            {
                return NotFound();
            }

            return Ok(enterprise);
        }

        /// <summary>
        /// Lista todos os tipos de Empresas
        /// </summary>                
        [Authorize("Bearer")]
        [HttpGet("types")]
        public ActionResult<IEnumerable<EnterpriseType>> GetEnterpriseTypes()
        {
            var listEnterpriseTypes = _enterpriseService.FindEnterpriseTypesAll();

            if (listEnterpriseTypes.Count == 0)
            {
                return NotFound();
            }

            return Ok(listEnterpriseTypes);
        }


        /// <summary>
        /// Lista todas as Empresas pelo typeId
        /// </summary>
        [Authorize("Bearer")]
        [HttpGet("types/typeId/{id}")]
        public ActionResult<IEnumerable<Enterprise>> GetEnterpriseByTypeId([FromRoute] int id)
        {
            var listEnterprise = _enterpriseService.FindEnterpriseByTypeId(id);

            if (listEnterprise.Count == 0)
            {
                return NotFound();
            }

            return Ok(listEnterprise);
        }

        /// <summary>
        /// Lista todas as Empresas pelo typeName
        /// </summary>
        [Authorize("Bearer")]
        [HttpGet("types/typeName/{name}")]
        public ActionResult<IEnumerable<Enterprise>> GetEnterpriseByTypeName([FromRoute] string name)
        {
            var listEnterprise = _enterpriseService.FindEnterpriseByTypeName(name);

            if (listEnterprise.Count == 0)
            {
                return NotFound();
            }

            return Ok(listEnterprise);
        }        
    }
}